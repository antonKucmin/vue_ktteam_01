import { Router } from 'express';
import { elasticSearch } from "../config.json"

export default ({ config, db }) => {
    
    let catalog = Router();
	let request = require('request');
    let obJson = [];
    var hits = '';

    catalog.all('/*', (req, res) => {
        var userURL = req.path.split('/').slice(1); // записываем в массив по частям products, product, _search
        if(userURL.length === 3 && userURL[0].length > 0 && userURL[1].length > 0 && userURL[2] === '_search') {// валидация 
            if (req.method === 'GET' || req.method === 'POST') { // если запрос get или post то работаем дальше
                request({
                    method: req.method, //  текущий метод запроса
                    url: elasticSearch + req.url, // products/product/_search + get тело
                    body: req.body, // тело post запроса 
                    json: true // сразу преобразует в json форат
                }, function (error, response, body) {
                    if (!error && response.statusCode == 200) { // если нет ошибки и статус 200                          
                        obJson = body.hits.hits.map(function(arr) {
                            return arr._source;
                        })
                        hits = body.hits; 
                        hits.hits = obJson;

                        res.send(hits);
                    } else {
                        // если ошибка запроса тела в elastic 
                        res.status(response.statusCode).send({error : response.body.error}); // вывод ошибки                        
                    }
                });
            } else {
                res.status(404).send('Method is not supported');// неверный метод
            }
        } else { 
            res.status(404).send('URL not filled correctly');// неверный url
        }
    });
	return catalog;
}  