
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

import { APIservice } from './catalog' 
const apiService = new APIservice();

export const store = new Vuex.Store({
  state: {
    count: 0,
    showModal: true,
    products: [],
    pageNumber: 0,
    maxMin: ''
  },
  mutations: {
    increment(state, payload) {
      state.count += payload;
    },
    showModal(state, a) {
      state.showModal = a;
    },
    getProducts(state, sort) {
      apiService.getProducts(sort).then((data) => {
        state.products = data;
      });
    },
    
    pageNumbers(state, a) {
      if(a > 0){
        state.pageNumber += a;
      } else if (a == 0) {
        state.pageNumber = 0;
      } else {
        state.pageNumber -= -a;
      }
    },
    sortMaxMin(state, a) {
      state.maxMin = a;
    }
    
  },
  actions: {
    increment({commit}, a) {
      commit('increment', a);
    },
    showModal({commit}, a) {
      commit('showModal', a);
    },
    getProducts({commit}, sort) {
      commit('getProducts', sort);
    },
    pageNumbers({commit}, a) {
      commit('pageNumbers', a);
    },
    sortMaxMin({commit}, a){
      commit('sortMaxMin', a);
    }
    
  },
  
});


