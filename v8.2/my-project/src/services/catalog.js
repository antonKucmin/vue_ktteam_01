
export class APIservice {
    getProducts = function(sort) {  
         let url = 
         `http://localhost:8079/api/catalog/products2/product/_search?from=${sort.from}&sort=${sort.sort}`;
         if(sort.size) {
            url = `http://localhost:8079/api/catalog/products2/product/_search?from=${sort.from}&size=${sort.size}&sort=${sort.sort}`;
         } 
        return fetch(url).then((response) => {
            if(response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok');
        });
    };
}



