var arrProduct_isTimer = [
    {
        brand: 'bmwX5',
        seconds: 30,
        minutes: 30,
        hours: 30,
        product: 5
    },
    {
        brand: 'bmwX6',
        seconds: 10,
        minutes: 2,
        hours: 20,
        product: 4
    },
    {
        brand: 'hyndai',
        seconds: 5,
        minutes: 1,
        hours: 0,
        product: 3
    }];

    
arrProduct_isTimer.forEach(element => {
    productData(element);
});

function productData(data) {
    var mark = data;
    $('#'+ mark.brand).text(mark.product);    
    
    var timer = setInterval (function() {

        if (mark.seconds > 0) {
            mark.seconds--;        
        } else {
            mark.seconds = 59;
            mark.minutes--;       
        }
        if (mark.minutes < 0) {
            mark.minutes = 59;
            mark.hours--;        
        }       
        if (mark.seconds <= 0 && mark.minutes == 0 && mark.hours == 0) {            
          clearInterval(timer);          
          mark.seconds = 0;
        }  
    var secondNull = mark.seconds < 10  ? '0' : '';
    var minutesNull = mark.minutes < 10 ? '0' : '';
    var hoursNull = mark.hours < 10 ? '0' : '';
        
    $('.'+ mark.brand).text(hoursNull + mark.hours 
            + ' : ' +  minutesNull + mark.minutes 
            + ' : ' + secondNull + mark.seconds);  
    },1000);
}
$('.button').click(function(){
    $('.modalWindow').css('display', 'flex');
});

$('.buttonClose').click(function(){
    $('.modalWindow').css('display', 'none');
});

$('.modalWindow').click(function(e){    
    if ($('.modalWindow').is(e.target)) {
        $('.modalWindow').css('display', 'none');
    }
    
});





