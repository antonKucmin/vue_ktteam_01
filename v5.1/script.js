var arrProduct_isTimer = [
    {
        brand: 'bmwX5',
        seconds: 30,
        minutes: 30,
        hours: 30,
        product: 5
    },
    {
        brand: 'bmwX6',
        seconds: 20,
        minutes: 20,
        hours: 20,
        product: 4
    },
    {
        brand: 'hyndai',
        seconds: 10,
        minutes: 10,
        hours: 10,
        product: 3
    }];
    
arrProduct_isTimer.forEach(element => {
    productData(element);
    console.log(element);
});

function productData(data) {
    var mark = data;
    $('#'+ mark.brand).text(mark.product);

    var timer = setInterval (function() {
        $('.'+ mark.brand).text(mark.hours 
            + ' : ' + mark.minutes 
            + ' : ' + mark.seconds);        

        if (mark.seconds > 0) {
            mark.seconds--;        
        } else {
            mark.seconds = 59;
            mark.minutes--;       
        }
        if (mark.minutes < 0) {
            mark.minutes = 59;
            mark.hours--;        
        }       
        if (mark.seconds < 0 && mark.minutes == 0 && mark.hours == 0) {
            clearInterval(timer);
        }
    },1000 );
}
$('.button').click(function(){
    $('.modalWindow').css('display', 'block');
});

$('.buttonClose').click(function(){
    $('.modalWindow').css('display', 'none');
});
$('.modalWindow').click(function(e){
    var modal = $('.modalWindow');    
    if (modal.is(e.target) && modal.has(e.tartet).length === 0 ) {
        $('.modalWindow').css('display', 'none');
    }
    
})