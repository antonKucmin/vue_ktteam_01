
Vue.component('card-component', {
  
   template: `<div class="card">
            <div class="imageProduct">
                <img src="image/312278506_w0_h0_cid2517180_pid213657573-88a524ad.jpg" alt="">
                <div class="information">
                    <div class="taimer">
                        <timer-component></timer-component>
                        <p>часов минут секунд</p>
                    </div>
                    <div class="leftover">
                        <p><span id="bmwX5"></span> Шт</p> 
                        <p>Осталось</p>
                    </div>
                </div>
            </div>
            <div class="carBrand textLeft">
                <p>BMW X5 "07-13"</p>
            </div>
            <div class="product textLeft">
                <p>Пороги OEM</p>
            </div>
            <div class="codProduct">
                <span>Код Товара: 15409</span> 
            </div>
            <div class="manufacturer textLeft">
                <p>Производитель:OEM-Tuning</p>
            </div>
            <div class="availability textLeft">
                <div><img src="image/verification-of-delivery-list-clipboard-symbol.png" alt=""></div>
                <p>В наличии (<span class="product_balance_bmw_x5"></span>-кт.)</p>
            </div>
            <div class="super">
                <div class="discount">
                    <p><span>-15%</span><br>скидка</p>
                </div>
                <div class="superPrice">
                    <p>супер цена</p>
                </div>
                <div class="discout_cards">
                    <p>+ скидка по карте<br>постоянного покупатуля</p>
                </div>
            </div>
            <div class="price textLeft">
                <p class="oldPrice">Обычная цена: <span> 19 584 руб</span></p>
                <p class="newPrice">Цена: <span>16 646 руб</span></p>
            </div>
            <div class="button">
                <input id="button" type="submit" value="Купить">
            </div>
            <div class="more">
                <div class="more_1">
                    <div> <img src="image/heart-outline.png" alt=""></div>
                    <span>Отложить</span>
                </div>
                <div class="more_1">
                    <div><img src="image/next.png" alt=""></div> 
                    <span>Подробнее</span>
                </div>
            </div>
        </div>
  `
});

new Vue({ el: '.cards_3' });
