Vue.component('hello-component', {
   data: function() {
      return {
         name: 'HELLO WORD'
      }
   },
   template: '<p> {{name}} </p>'
});

new Vue({ el: '#hello-component' });

